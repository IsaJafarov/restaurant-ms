package com.restaurant.web.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.restaurant.domain.Restaurant;
import com.restaurant.security.jwt.JWTFilter;
import com.restaurant.security.jwt.TokenProvider;
import com.restaurant.service.RestaurantService;
import com.restaurant.web.rest.vm.LoginVM;
import org.checkerframework.checker.nullness.Opt;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

/**
 * Controller to authenticate users.
 */
@RestController
@RequestMapping("/api")
public class UserJWTController {

    private final TokenProvider tokenProvider;

    private final AuthenticationManagerBuilder authenticationManagerBuilder;

    private final JavaMailSender javaMailSender;
    private final RestaurantService restaurantService;
    private final PasswordEncoder passwordEncoder;

    public UserJWTController(TokenProvider tokenProvider, AuthenticationManagerBuilder authenticationManagerBuilder, JavaMailSender javaMailSender, RestaurantService restaurantService, PasswordEncoder passwordEncoder) {
        this.tokenProvider = tokenProvider;
        this.authenticationManagerBuilder = authenticationManagerBuilder;
        this.javaMailSender = javaMailSender;
        this.restaurantService = restaurantService;
        this.passwordEncoder = passwordEncoder;
    }


    @PutMapping("/otp")
    public HttpStatus getOTP(@RequestBody Map<String, String> body){
        String email = body.get("email");
        String otp = UUID.randomUUID().toString();

        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(email);
        message.setSubject("Your OTP");
        message.setText( otp );
        javaMailSender.send(message);

        Optional<Restaurant> optionalRestaurant = restaurantService.findByEmail(email);
        optionalRestaurant.ifPresent(restaurant -> restaurantService.updateOTP(restaurant.getId(), passwordEncoder.encode(otp))  );

        return HttpStatus.OK;
    }

    @PostMapping("/authenticate")
    public ResponseEntity<JWTToken> authorize(@Valid @RequestBody LoginVM loginVM) {

        UsernamePasswordAuthenticationToken authenticationToken =
            new UsernamePasswordAuthenticationToken(loginVM.getUsername(), loginVM.getPassword());

        Authentication authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        boolean rememberMe = (loginVM.isRememberMe() == null) ? false : loginVM.isRememberMe();
        String jwt = tokenProvider.createToken(authentication, rememberMe);

        Optional<Restaurant> optionalRestaurant = restaurantService.findByEmail(loginVM.getUsername());
        optionalRestaurant.ifPresent(restaurant -> restaurantService.updateOTP(restaurant.getId(), ""));


        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(JWTFilter.AUTHORIZATION_HEADER, "Bearer " + jwt);
        return new ResponseEntity<>(new JWTToken(jwt), httpHeaders, HttpStatus.OK);
    }

    /**
     * Object to return as body in JWT Authentication.
     */
    static class JWTToken {

        private String idToken;

        JWTToken(String idToken) {
            this.idToken = idToken;
        }

        @JsonProperty("id_token")
        String getIdToken() {
            return idToken;
        }

        void setIdToken(String idToken) {
            this.idToken = idToken;
        }
    }
}
